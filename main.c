#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <png.h>

#define MATCH_COUNT 16

struct ColorBlockMatch {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	unsigned char blockId;
	unsigned char blockMetadata;
};

#define TRY_WRITE(ch, f) \
	do \
	{ \
		if (fputc((ch), f) == EOF) \
		{ \
			fprintf(stdout, "Error when writing data to file\n"); \
			return 0; \
		} \
	} \
	while(0)

int main(int argc, char **argv)
{
	int iter1, iter2;
	char *path_to_config, *path_to_png, *path_to_map;
	png_image image;
	png_bytep image_buffer;
	FILE *file;

	struct ColorBlockMatch matches[MATCH_COUNT];

	if (argc == 3)
	{
		path_to_config = "config.txt";
		path_to_png = argv[1];
		path_to_map = argv[2];
	}
	else if (argc == 4)
	{
		path_to_config = argv[1];
		path_to_png = argv[2];
		path_to_map = argv[3];
	}
	else
	{
		fprintf(stdout, "Usage: %s "
		                "[path-to-config (default: \"config.txt\")] "
		                "<path-to-png> "
		                "<path-to-map>\n", argv[0]);
		return 0;
	}

	file = fopen(path_to_config, "r");
	if (!file)
	{
		fprintf(stdout, "Could not open config file\n");
		return 0;
	}

	for (iter1 = 0; iter1 < MATCH_COUNT; ++iter1)
	{
		if (fscanf(file, "%d,%d,%d:%d:%d",
		           &(matches[iter1].red),
		           &(matches[iter1].green),
		           &(matches[iter1].blue),
		           &(matches[iter1].blockId),
		           &(matches[iter1].blockMetadata)) != 5)
		{
			fprintf(stdout, "Config file is broken\n", iter1);
			return 0;
		}
	}
	fclose(file);

	memset(&image, 0, (sizeof image));
	image.version = PNG_IMAGE_VERSION;

	if (!png_image_begin_read_from_file(&image, path_to_png))
	{
		fprintf(stdout, "Could not open image file\n");
		return 0;
	}

	image.format = PNG_FORMAT_RGB;
	image_buffer = (png_bytep) malloc(PNG_IMAGE_SIZE(image));

	if (!image_buffer)
	{
		fprintf(stdout, "Could not allocate memory for buffer\n");
		return 0;
	}

	if (!png_image_finish_read(&image, 0, image_buffer, 0, 0))
	{
		fprintf(stdout, "PNG file is broken\n");
		return 0;
	}

	file = fopen(path_to_map, "w");
	if (!file)
	{
		fprintf(stdout, "Could not open map file\n");
		return 0;
	}
	TRY_WRITE((image.width >> 8) & 0xFF, file);
	TRY_WRITE(image.width & 0xFF, file);
	TRY_WRITE((image.height >> 8) & 0xFF, file);
	TRY_WRITE(image.width & 0xFF, file);
	for (iter1 = 0; iter1 < image.width * image.height; ++iter1)
	{
		for (iter2 = 0; iter2 < MATCH_COUNT; ++iter2)
		{
			if (    matches[iter2].red   == image_buffer[3 * iter1] &&
			        matches[iter2].green == image_buffer[3 * iter1 + 1] &&
			        matches[iter2].blue  == image_buffer[3 * iter1 + 2])
			{
				TRY_WRITE(matches[iter2].blockId, file);
				TRY_WRITE(matches[iter2].blockMetadata, file);
				iter2 = MATCH_COUNT + 1;
			}
		}

		if (iter2 == MATCH_COUNT)
		{
			fprintf(stdout, "No match for color: [%d,%d,%d]\n"
			                "at position: [%d,%d]\n",
			        image_buffer[3 * iter1],
			        image_buffer[3 * iter1 + 1],
			        image_buffer[3 * iter1 + 2],
			        iter1 % image.width,
			        iter1 / image.width);
			return 0;
		}
	}
	fclose(file);
	return 0;
}
